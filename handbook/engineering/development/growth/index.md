---
url: https://about.gitlab.com/handbook/engineering/development/growth/
last-read-on: 2020-04-20
---

# [Growth Sub-department](https://about.gitlab.com/handbook/engineering/development/growth/)

## Questions

- [ ] What is BCP?
- [ ] Who is the Data Team?
- [ ] Who are our Product Team counterparts?
- [ ] Is there a separate monthly planning meeting? If so, when is the next one?
- [ ] What do the “M-1” & “M+1” of “M-1, 4th: Draft of issues” & “M+1, 4th: Public Retrospective” mean?

## To-dos

- [ ] Learn the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary)
- [ ] Read [“velocity over predictability”](https://about.gitlab.com/handbook/engineering/#velocity-over-predictability)
- [ ] Read [“weekly experimentation cadence”](https://about.gitlab.com/handbook/product/growth/#weekly-growth-meeting)
- [ ] Read Andrew Chen’s [“How to build a growth team”](https://andrewchen.co/how-to-build-a-growth-team/)
- [ ] Explore some of the [Common Links](https://about.gitlab.com/handbook/engineering/development/growth/#common-links)

## Comments

## Miscellaneous
