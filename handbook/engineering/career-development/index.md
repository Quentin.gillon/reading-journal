---
url: https://about.gitlab.com/handbook/engineering/career-development/
last-read-on: 2020-05-21
---

# [Engineering Career Development](https://about.gitlab.com/handbook/engineering/career-development/)

## General Notes

- Three components of career development
  1. Structure
     - Career coaching conversation with manager
  2. Individual initiative
     - Step into a position of informal or formal leadership
     - Should have career development conversation with manager at least once a quarter
  3. Opportunity
     - There are lots of opportunities at GitLab
     - Internal promotions are generally a first option
- Individual contribution vs. management
  - Once at a senior-level & want to progress, must decide between purely technical or managerial tracks
  - Staff-level engineer is equivalent (in pay & prestige) to engineering manager
- Can try out the management track with simpler, smaller managerial tasks
- There may be times when a temporary manager is needed
  - can choose if they want to continue pursuing management or if they want to go back to being individual contributor
  - Acting Manager vs. Interim Manager
    - Acting is just temporarily filling in, then will likely go back to IC
    - Interim is for people who are pursuing management long term
- To become a Senior Engineer:
  - Receive fewer trivial comments on MRs
  - Demonstrate attention to detail
  - Receive far fewer major comments on MRs
  - Demonstrate an understanding of the application’s architecture
  - Select from proven patterns to solve engineering needs
  - Come up with simple solutions to complex problems
  - Demonstrate an ability to effectively manage complexity

## Questions

- [ ] …
  - **A:** …

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
