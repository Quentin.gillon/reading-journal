---
url: https://about.gitlab.com/handbook/product-development-flow/
last-read-on: 2020-05-21
---

# [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)

## General Notes

- Read the [Iteration Strategies section](https://about.gitlab.com/handbook/product-development-flow/) on 2020-05-21

## Questions

- [ ] …
  - **A:** …

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
