---
url: https://about.gitlab.com/handbook/product/
last-read-on: DATE
---

# [Product](https://about.gitlab.com/handbook/product/)

## General Notes

- Looking at just the [“The Minimal Viable Change (MVC)” section](https://about.gitlab.com/handbook/product/#the-minimal-viable-change-mvc) on 2020-05-21
  - Read up on the [“Not like this, like this!” illustration of iterative/agile development](https://blog.crisp.se/2016/01/25/henrikkniberg/making-sense-of-mvp)
    - Step one, skateboard: Earliest Testable Product (ETP)
      - customer is unlikely to be happy with this – not what he/she ordered!
      - we’re not trying to make customer happy at this point
      - main goal is just to learn!
      - key question: “What is the cheapest & fastest way we can start learning?”
      - testing a prototype on even a single user will teach you more than nothing
    - Step two, scooterboard: build upon what you learned in step one
    - Step three, bicycle: building upon learnings from previous steps
    - Step four, motorcycle: maybe this actually fulfills the customer’s needs and we can stop early!
      - it’s all about finding the simples & cheapest way of solving the customer’s problem
      - each iterative prototype along the way guides us down the path by providing lots of lessons learned
    - Step five, convertible: better than originally planned because of incremental learnings along the way
    - What’s your skateboard? Very important question!
    - Let’s split MVP up into three different things:
      1. Earliest Testable Product (like the skateboard version)
      2. Earliest Usable Product (like the bicycle)
      3. Earliest Lovable Product (like the motorcycle) – first truly marketable version of the product
  - Advantages of the MVC approach:
    - prevents over-engineering
    - forces everyone involved to search for simplest solution possible
    - work towards a solution good enough for 80% of the market
    - easier to review than bigger changes
    - easier to rollback/undo than larger changes
    - less likely to conflict with other development efforts
    - takes less time to get it out there and for everyone to start benefitting from it
    - further development is driven by greater input than a PM or small team could provide

## Questions

- [ ] …
  - **A:** …

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
