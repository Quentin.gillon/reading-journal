---
url: https://about.gitlab.com/handbook/competencies/
last-read-on: 2020-05-29
---

# [Competencies](https://about.gitlab.com/handbook/competencies/)

## General Notes

- Great [list](https://about.gitlab.com/handbook/competencies/#list) of competencies to work on.
  - I briefly reviewed the following items:
    - [Managers of One](https://about.gitlab.com/handbook/leadership/#managers-of-one)
    - [Engineering (Technical) Competencies](https://about.gitlab.com/handbook/engineering/career-development/career-matrix.html#technical-competencies)

## Questions

- [ ] …
  - **A:** …

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
