---
url: https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/
last-read-on: 2020-05-21
---

# [GitLab Inc (US) Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/)

## General Notes

- Just reading the [“401k Plan” section](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan) on 2020-05-21
  - Eligible as of hire date – there is no auto-enrollment, though
  - ADP will automatically block contributions once yearly limit is reached
  - GitLab matches 50% of the first 6% of allocated earnings with a yearly cap of $1,500 USD.
  - GitLab (employer) contributions vest according to a schedule:

    | Years of Vesting Service | Vesting Percentage |
    | ------------------------ | ------------------ |
    | < 1                      | 0%                 |
    | 1 to < 2                 | 25%                |
    | 2 to < 3                 | 50%                |
    | 3 to < 4                 | 75%                |
    | 4+                       | 100%               |

  - As an example of how the vesting works, if I work here for just about 2 years and then leave, and my salary while I was here was $50,000, and I put in $3,000/year to get GitLab’s maximum match of $1,500, I would have put in $6,000 and GitLab would have matched it with $3,000, but since only 25% had vested, I would actually have $6,750 (my $6,000 + 25% of their $3,000).

## Questions

- [ ] I feel like I’ve probably asked this already somewhere else (maybe in the #total-rewards channel in Slack), but what is the quarterly “true up” that Payroll & People Operations will conduct if you defer more than 6% for each paycheck?
  - **A:** …

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
