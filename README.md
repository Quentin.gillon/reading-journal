# 📓 Reading Journal

## Why?

I find that I read/focus better if I’m taking notes along the way. I also tend to forget which pages I’ve read & which I haven’t & this will help me to keep track.

## How Does This Work?

The idea is to keep a 1:1 correlation between pages on about.gitlab.com and Markdown pages in this journal.

For each about.gitlab.com page read, create/update a page here with the following:

1. Title of page
1. URL of page
1. Date the page was last read/reviewed
1. A section of Questions
1. A section of To-dos
1. A section of Comments
1. Any other miscellaneous datapoints, such as links to related pages or relevant MRs I’ve authored
